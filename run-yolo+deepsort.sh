#!/bin/bash
# Need to ensure certain requirements are met. Not this assumes that this will be run in the provided docker image and only needs to mess/modify it.

# Certain commands needed to finish setting up docker correctly
apt-get update
apt-get install -y libsm6 libxext6 libxrender-dev xauth ffmpeg # libzmq3-dev

pip3 show tensorflow | grep "Version: 2"
if [ $? -eq 0 ] || [ $? -eq 1 ] #0 if it was found and 1 if it doesn't exist
then
	pip3 install tensorflow-gpu==1.14 #ensure right tensorflow model version because PAIN
fi
pip3 install numpy
pip3 install sklearn
pip3 install pillow
pip3 install -U opencv-python
pip3 install Keras
pip3 install imutils
pip3 install pafy
pip3 install -U youtube-dl
pip3 install vidgear
cd ./Yolo+DeepSort/
python3 demo.py
