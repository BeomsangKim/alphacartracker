#!/bin/bash

xhost +
sudo docker run --rm -it \
	-e DISPLAY=$DISPLAY \
	-p 8888:8888 \
	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	--volume="$HOME/Documents/summerstudio/alphacartracker:/tf/studio" \
	--volume="$HOME/.Xauthority:/root/.Xauthority" \
	--hostname $(hostname) \
	--env="QT_X11_NO_MITSHM=1" \
	--privileged \
	--net host \
	benthepleb/summerstudio2020:latest \
	bash
