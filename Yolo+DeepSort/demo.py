
from __future__ import division, print_function, absolute_import

#for filenaming
from datetime import datetime

import os
from timeit import time
import warnings
import sys
import cv2
import numpy as np
from PIL import Image
from yolo import YOLO
#from tqdm import tqdm
from cv2 import CAP_PROP_FRAME_COUNT
import imutils
import csv

from deep_sort import preprocessing
from deep_sort import nn_matching
from deep_sort.detection import Detection
from deep_sort.tracker import Tracker
from tools import generate_detections as gdet
#from deep_sort.detection import Detection as ddet

warnings.filterwarnings('ignore')

# Custom video streaming stuff
from vidgear.gears import CamGear
from vidgear.gears import WriteGear

track_list = []
total=0

# Create a tracking dictionary
track_specific_id = {}
track_specific_id_regions = {}

# Frame skip stuff
frame_skip_state = False
frame_skip = 0
skip_limit = 6

# Display skip stuff
display_skip_state = False #needed when streaming
display_skip = 0
d_skip_limit = 6

# Target area(s)
# This is hardcoded atm, need to fix/update in future.
target_areas = [[1200,700,1700,1000],[500,200,800,400]]
target_areas2 = [[1200,700,1700,1000],[400,700,700,1000]]
target_areas3 = [[400,700,700,1000],[1300,250,1600,400]]

# areas for video 2
v2_target_areas = [[1300,200,1700,400],[1600,1400,2200,1800]]
v2_target_areas2 = [[3200,1000,3700,1400],[2400,1800,3200,2100]]

# areas for video 2
v3_target_areas = [[3200,1900,3600,2100],[100,400,800,800]]
v3_target_areas2 = [[1800,400,2200,800],[3000,1700,3400,1900]]

target_paths = [v3_target_areas,v3_target_areas2]

youtube_sources = ['https://www.youtube.com/watch?v=ew0rBDYLxLc', 'https://www.youtube.com/watch?v=MNn9qKG2UFI', 'https://www.youtube.com/watch?v=jjlBnrzSGjc']

def Counting_People(yolo):
	global track_list
	global total

	# Custom globals
	global track_specific_id
	global track_specific_id_regions
	
	# Frame skip declarations
	global frame_skip_state
	global frame_skip
	global skip_limit
	
	# Display skip declarations
	global display_skip_state
	global display_skip
	global d_skip_limit

	#global total_frame
	currentFrame = 0
	#global currentFrame
	# Definition of the parameters
	max_cosine_distance = 0.3
	nn_budget = None
	nms_max_overlap = 1.0

	# deep_sort
	model_filename = 'model_data/mars-small128.pb'
	encoder = gdet.create_box_encoder(model_filename, batch_size=1)

	metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_cosine_distance, nn_budget)
	tracker = Tracker(metric)
	writeVideo_flag = False #changed this to false while I attempt to run on a stream
	write_video = True # added to make new video writing which should require ffmpeg

	# Old define your video file path 
	#video_capture = cv2.VideoCapture('./cut-car-vid.mkv')
	
	#CamGear Options

	#video_capture = CamGear('./cut-car-vid.mkv').start()

	# Define stream url
	video_capture = CamGear(source=youtube_sources[2], y_tube=True).start() # YouTube Video URL as input

	# Define write output stuff
	if write_video:
		#output_params = {'-vcodec': 'h264_vaapi', '-vaapi_device':'/dev/dri/renderD128', '-vf':'format=nv12,hwupload'} #Hardware encoder attempt
		output_params = {"-vcodec":"libx265", "-crf": 23, "-preset": "slow"} #Software Encoder
		now = datetime.now()
		current_time = now.strftime("%H:%M:%S")
		writer = WriteGear(output_filename =  'output_videos/' + current_time + ' - Output.mkv', compression_mode = True, logging = False, **output_params)

	#total_frame = int(video_capture.get(cv2.CAP_PROP_FRAME_COUNT))

	if writeVideo_flag:
		# Define the codec and create VideoWriter object
		w = int(video_capture.get(3))
		h = int(video_capture.get(4))
		fourcc = cv2.VideoWriter_fourcc(*'MJPG')
		out = cv2.VideoWriter('output.avi', fourcc, 15, (w, h))
		list_file = open('detection.txt', 'w')
		frame_index = -1

	fps = 0.0
	currentFrame = 0
	frame_number=1
	while True:
		#The old video caputre thingy
		#ret, frame = video_capture.read()  # frame shape 640*480*3
		#if ret != True:
		#	break;
		
		frame = video_capture.read()
		if frame is None:
		    break;

		#attempt to skip frames to make life less painful XD
		if frame_skip_state and frame_skip <= skip_limit:
			frame_skip = frame_skip +1
			print("skipping")
			#cv2.waitKey(100)
			continue;
		else:
			frame_skip = 0
			print("not skipped")

		t1 = time.time()
		currentFrame += 1
		# Little hack to skip the start of a video
		if currentFrame < 300:
			continue
		image = Image.fromarray(frame)
		boxs = yolo.detect_image(image)
		print(boxs)
		# print("box_num",len(boxs))
		features = encoder(frame, boxs)

		# score to 1.0 here).
		detections = [Detection(bbox, 1.0, feature) for bbox, feature in zip(boxs, features)]

		# Run non-maxima suppression.
		boxes = np.array([d.tlwh for d in detections])
		scores = np.array([d.confidence for d in detections])
		indices = preprocessing.non_max_suppression(boxes, nms_max_overlap, scores)
		detections = [detections[i] for i in indices]
		#print(detections)

		# Call the tracker
		tracker.predict()
		tracker.update(detections)
		
		# Draw detection areas
		for path_num, target_path in enumerate(target_paths, start=1):
			for rect_num, target_rect in enumerate(target_path, start=1):
				cv2.rectangle(frame, (int(target_rect[0]), int(target_rect[1])), (int(target_rect[2]), int(target_rect[3])), (200, 40, 200), 2)
				cv2.putText(frame, str(path_num), (int(target_rect[0]), int(target_rect[1])), 0, 5e-3 * 200, (0, 255, 0), 2)
		#for target_rect in target_areas1:
		#	cv2.rectangle(frame, (int(target_rect[0]), int(target_rect[1])), (int(target_rect[2]), int(target_rect[3])), (200, 40, 200), 2)
		#	cv2.putText(frame, str(track.track_id), (int(bbox[0]), int(bbox[1])), 0, 5e-3 * 200, (0, 255, 0), 2)

		for track in tracker.tracks:
			if track.is_confirmed() and track.time_since_update > 1:
				continue
				# count=0
			bbox = track.to_tlbr()
			a=int(bbox[0])
			b=int(bbox[1])
			c=int(bbox[2])
			d=int(bbox[3])
			# Store Detections for MOTA chalenge Format
			list1=[frame_number,track.track_id,a,b,c,d,-1,-1,-1,-1]

			with open("res.txt", "a") as fp:
				wr = csv.writer(fp, dialect='excel')
				wr.writerow(list1)

			# Add the id to the dictionary
			if track.track_id not in track_specific_id:
				track_specific_id[track.track_id] = []
				track_specific_id_regions[track.track_id] = [[False for i in roi] for roi in target_paths]
				#for path_num, path_regions in enumerate(target_paths, start=0):
				#	print(path_num)
				#	track_specific_id_regions[track.track_id][path_num] = [False for i in path_regions]
				#track_specific_id_regions[track.track_id] = [False for i in target_areas]

			# Add the point to the dictionary for that ID
			track_specific_id[track.track_id].append([((bbox[0]+bbox[2])/2),((bbox[1]+bbox[3])/2)])
			#print(track.track_id)
			#print(track_specific_id[track.track_id])
			# Draw the path that the given ID has taken
			pts = np.array(track_specific_id[track.track_id], np.int32)
			pts = pts.reshape((-1,1,2))
			cv2.polylines(frame,[pts],False,(160,32,240),thickness=3,lineType=cv2.LINE_AA)
			# Draw the detected area and ID of the object
			cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), (255, 255, 255), 2)
			cv2.putText(frame, str(track.track_id), (int(bbox[0]), int(bbox[1])), 0, 5e-3 * 200, (0, 255, 0), 2)

			# Better path detection
			for path_num, target_path in enumerate(target_paths, start=0):
				if False not in track_specific_id_regions[track.track_id][path_num]:
					cv2.circle(frame, (int(track_specific_id[track.track_id][-1][0]), int(track_specific_id[track.track_id][-1][1])), 40, (0, 0, 255), 3)
					break
				latest_area = track_specific_id_regions[track.track_id][path_num].index(False)
				if track_specific_id[track.track_id][-1][0] > target_path[latest_area][0] and \
					track_specific_id[track.track_id][-1][1] > target_path[latest_area][1] and \
					track_specific_id[track.track_id][-1][0] < target_path[latest_area][2] and \
					track_specific_id[track.track_id][-1][1] < target_path[latest_area][3]:
					track_specific_id_regions[track.track_id][path_num][latest_area] = True
				#for rect_num, target_rect in enumerate(target_path, start=0):

			# Detect if the ID is within a certain area
			# want to iterate over the areas, breaking if its in a area out of order
#			for i in range(len(target_areas)):
#				if track_specific_id_regions[track.track_id][i]:
#					continue
#				else:
#					if track_specific_id[track.track_id][-1][0] > target_areas[i][0] and \
#						track_specific_id[track.track_id][-1][1] > target_areas[i][1] and \
#						track_specific_id[track.track_id][-1][0] < target_areas[i][2] and \
#						track_specific_id[track.track_id][-1][1] < target_areas[i][3]:
#						track_specific_id_regions[track.track_id][i] = True
#						break
#					else:
#						break
			#draw a circle on those which have had this movement
#			if False not in track_specific_id_regions[track.track_id]:
#				cv2.circle(frame, (int(track_specific_id[track.track_id][-1][0]), int(track_specific_id[track.track_id][-1][1])), 40, (0, 0, 255), 3)

			track_list.append(track.track_id)

		frame_number+=1
		for det in detections:
			bbox = det.to_tlbr()
			cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), (255, 0, 0), 2)

		if write_video:
			writer.write(frame)

		# Added a section to allow for display skipping in situations of display latency/compression issues
		if not display_skip_state:
			cv2.imshow('', frame)
		elif display_skip_state and display_skip <= d_skip_limit:
			display_skip = display_skip +1
			print("skipping display")
		else:
			display_skip = 0
			print("show display frame")
			frame = imutils.resize(frame, width=650)
			cv2.imshow('', frame)

		if writeVideo_flag:
			# save a frame
			out.write(frame)
			frame_index = frame_index + 1

			list_file.write(str(frame_index) + ' ')

			if len(boxs) != 0:
				for i in range(0, len(boxs)):
					list_file.write(str(boxs[i][0]) + ' ' + str(boxs[i][1]) + ' ' + str(boxs[i][2]) + ' ' + str(
						boxs[i][3]) + ' ')
			list_file.write('\n')

		fps = (fps + (1. / (time.time() - t1))) / 2
		# print (my_list)
		total = (len(set(track_list)))
		print("fps= %f" % (fps))
		# Press Q to stop!
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break

	cv2.destroyAllWindows()
	video_capture.stop()
	if write_video:
		writer.close()
	if writeVideo_flag:
		out.release()
		list_file.close()


if __name__ == '__main__':
	Counting_People(YOLO())
