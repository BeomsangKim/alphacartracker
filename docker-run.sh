#!/bin/bash

sudo docker run --rm -it \
	-e DISPLAY=$DISPLAY \
	-p 8888:8888 \
	--gpus all \
	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	--volume="$HOME/alphacartracker:/tf/studio" \
	--volume="$HOME/.Xauthority:/root/.Xauthority" \
	--hostname $(hostname) \
	--env="QT_X11_NO_MITSHM=1" \
	--privileged \
	--net host \
	ufoym/deepo:tensorflow-py36-cu100 \
	bash
