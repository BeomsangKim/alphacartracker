# AlphaCarTracker

The project is developed to detect illegal turns of car. It uses pretrained YOLO v.3 model for car recognition and DeepSORT for car tracking. As there is not any video of showing illegal turns of cars found, the model uses traffic videos as the feed to demonstrate its functionality.

# Dependencies

The code is compatible with Python 2.7 and 3. The following dependencies are needed to run the tracker:

NumPy

sklean

OpenCV

VidGear

Additionally, feature generation requires TensorFlow-1.4.0


# How to Run the code?

## Run the docker image:

1. For running locally/ For laptop use

Run this script in your local terminal
```
bash docker-run-local.sh
```

2. For running on cloud

Run this script in your cloud instance terminal should forward the x11 stream properly via SSH. It is also added an option to reduce displayed frames for situations which reduced bandwidth /latency.
```
bash docker-run.sh
```

## Go to the studio folder and clone the git
```
cd studio
git clone https://gitlab.com/BeomsangKim/alphacartracker.git
```

## Move to the git repo and excute the shell script
```
cd alphacartracker
./run-yolo+deepsort.sh
```

# Tip for running on gcloud virtual machine
Run this script to aid setup on Gcloud Virtual Machine. 

```
bash cloud_setup.sh
```

# Sources
**Dataset**

http://www.cvlibs.net/datasets/karlsruhe_objects/

**Related Reporsties**

https://github.com/nwojke/deep_sort

https://github.com/Qidian213/deep_sort_yolov3

https://github.com/Akhtar303/Vehicle-Detection-and-Tracking-Usig-YOLO-and-Deep-Sort-with-Keras-and-Tensorflow


