#!/bin/bash
# Need to ensure certain requirements are met. Not this assumes that this will be run in the provided docker image and only needs to mess/modify it.

# Certain commands needed to finish setting up docker correctly
apt-get update
apt-get install -y libsm6 libxext6 libxrender-dev xauth # libzmq3-dev

pip show tensorflow | grep "Version: 2"
if [ $? -eq 0 ] || [ $? -eq 1 ] #0 if it was found and 1 if it doesn't exist
then
	pip install tensorflow-gpu==1.14 #ensure right tensorflow model version because PAIN
fi
pip install numpy
pip install sklearn
pip install pillow
pip install -U opencv-python
pip install Keras
pip install imutils
pip install pafy
pip install -U youtube-dl
pip install vidgear
cd ./Yolo+DeepSort/
python demo.py
